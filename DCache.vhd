library IEEE;
use IEEE.std_logic_1164.all;  -- for the use of STD_LOGIC/_VECTOR
use IEEE.numeric_std.ALL;     -- for the use of to_integer function

entity DCache is
	port(	clk, DCRead, DCWrite:	in STD_LOGIC;
		DCHit:	out STD_LOGIC;
		DAddr: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		MDR: 	out STD_LOGIC_VECTOR(31 downto 0);	-- Instruction
		MDR_In:	in STD_LOGIC_VECTOR(31 downto 0);
		MDR_BlkIn:	in STD_LOGIC_VECTOR(127 downto 0);
		MDR_BlkOut:	out STD_LOGIC_VECTOR(127 downto 0));
end entity DCache;

architecture DCacheBhv of DCache is
	type Memory is array(0 to 255) of std_logic_vector(7 downto 0);
	signal Data : Memory;
	type Tag is array(0 to 511) of std_logic_vector(2 downto 0);
	signal DataTag : Tag:= (others=> (others=>'X'));
	type DBitType is array(0 to 511) of std_logic;
	signal DBit : DBitType;--:= (others=> (others=>'0'));
	signal temp_out,temp_in: std_logic_vector(31 downto 0);
	signal tempblk_in, tempblk_out: std_logic_vector(127 downto 0);

begin
		temp_out(31 downto 24) <= Data(to_integer(unsigned(DAddr))+0);
		temp_out(23 downto 16) <= Data(to_integer(unsigned(DAddr))+1);
		temp_out(15 downto 08) <= Data(to_integer(unsigned(DAddr))+2);
		temp_out(07 downto 00) <= Data(to_integer(unsigned(DAddr))+3);

		tempblk_in <= MDR_BlkIn;
		
		temp_in <= MDR_In;

	process(clk)
	begin 

	for n in 0 to 15 loop
		tempblk_out((127-8*n) downto (120-8*n)) <= Data(to_integer(unsigned(DAddr(7 downto 0))+n));
	end loop;

	if  rising_edge(clk) then 
	
	if DCRead = '1' then
		if DataTag(to_integer(unsigned(DAddr(7 downto 0)))) = (DAddr(10 downto 8) or "XXX" or "UUU") then
			DCHit <= '1';
			MDR <= temp_out;
		else
			DCHit <= '0';
			if DBit(to_integer(unsigned(DAddr(7 downto 0)))) = '1' then
				MDR_BlkOut <= tempblk_out;
			end if;
			for i in 0 to 15 loop
				Data(to_integer(unsigned(DAddr))+i) <= tempblk_in((127-8*i) downto (120-8*i));
				DataTag(to_integer(unsigned(DAddr(7 downto 0))+i)) <= DAddr(10 downto 8);
			end loop;
		end if;
	end if;

	if DCWrite = '1' then
		if DataTag(to_integer(unsigned(DAddr(7 downto 0)))) = (DAddr(10 downto 8) or "XXX" or "UUU") then
			DCHit <= '1';
			for x in 0 to 3 loop
				Data(to_integer(unsigned(DAddr(7 downto 0))+x)) <= MDR_In((31-8*x) downto (24-8*x));
				DataTag(to_integer(unsigned(DAddr(7 downto 0))+x)) <= DAddr(10 downto 8);
			end loop;
		else
			DCHit <= '0';
			if DBit(to_integer(unsigned(DAddr(7 downto 0)))) = '1' then
				MDR_BlkOut <= tempblk_out;
			end if;
			for i in 0 to 15 loop
				Data(to_integer(unsigned(DAddr))+i) <= tempblk_in((127-8*i) downto (120-8*i));
				DataTag(to_integer(unsigned(DAddr(7 downto 0))+i)) <= DAddr(10 downto 8);
			end loop;
		end if;

	end if;
	end if;
	end process;
end DCacheBhv; 

