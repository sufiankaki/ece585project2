
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;

entity ALU is
Port ( A,B : in std_logic_vector(31 downto 0);   		-- data input form the registers.
       ALUOut : inout std_logic_vector(31 downto 0); 		-- data output from the  ALU        
	ALUOp : in std_logic_vector( 3 downto 0);		-- ALU control line
       zero : out std_logic ;                          		-- zero output for branch instruction
       ovf : out std_logic := '0');                	-- overflow detection
end ALU;

architecture ALUbhv of ALU is  
begin
	process (AlUOp, A, B)
	begin 
	case AlUOp is

		when "0000" => 						-- nand operation
			ALUOut <= A nand B;                   
		when "0111" =>						-- xor operation
    			ALUOut <= A xor B;                     
		when "0010" => 						-- add operation                      
    			ALUOut <= A + B;
		when "0110" => 						-- sub operation
    			ALUOut <= A - B;             
		when "0101" => 						-- and operation
    			ALUOut <= A and B;     
		when "1010" =>						-- lui operation
			ALUOut(31 downto 16) <= B(15 downto 0);		
			ALUOut(15 downto 0) <= x"0000";        
		when others =>
    			ALUOut <= (others => 'X');
	end case;
	end process;

	zero <= '1' when ALUOut = x"00000000" else '0';
end ALUbhv;




